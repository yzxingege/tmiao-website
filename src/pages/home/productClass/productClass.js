import React from 'react';

const ProductClass = () => {
    return (
        <div id='container'>
            <div id="main" style={{width: '80%', marginLeft: '10%', paddingTop: 2, height: 30}}>
                <p style={{backgroundColor: "red", width: '10%', float: 'left'}}>
                    <span>
                        <img alt={"error"} src={require('./image/list.png')} style={{height: 22, width: 22, marginLeft: 8}}/>
                    </span>
                    <span style={{marginLeft: 10, fontSize: 16, color: "#fff"}}>商品分类</span>
                </p>
                <p style={{width: '85%'}}>
                    <b style={{marginLeft: 15, fontSize: 16}}>天猫超市</b>
                    <b style={{marginLeft: 15, fontSize: 16}}>天猫国际</b>
                    <b style={{marginLeft: 15, fontSize: 16}}>天猫会员</b>
                    <b style={{marginLeft: 15, fontSize: 16}}>电器城</b>
                    <b style={{marginLeft: 15, fontSize: 16}}>喵鲜生</b>
                    <b style={{marginLeft: 15, fontSize: 16}}>医药馆</b>
                    <b style={{marginLeft: 15, fontSize: 16}}>营业厅</b>
                    <b style={{marginLeft: 15, fontSize: 16}}>魅力惠</b>
                    <b style={{marginLeft: 15, fontSize: 16}}>飞猪旅行</b>
                    <b style={{marginLeft: 15, fontSize: 16}}>苏宁易购</b>
                </p>
            </div>
        </div>
    )
}
export default ProductClass