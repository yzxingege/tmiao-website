import React from 'react';
import Head from './head/head';
import HomeSearch from './homeSearch/homeSearch'
import ProductClass from './productClass/productClass'
import ProductMenu from './productMenu/productMenu'

const Index = () =>{
    return(
        <>
            <Head/>
            <HomeSearch/>
            <ProductClass/>
            <ProductMenu/>

        </>
    )
}
export default Index;