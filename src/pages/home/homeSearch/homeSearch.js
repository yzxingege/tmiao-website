import React from 'react';
import { Input } from 'antd';
const { Search } = Input;
//搜索
const HomeSearch = () =>{
    return(
        <div id='container'>
            <div id='main'>
              <div className='search'>
                  <div className='search-left'>
                      <img src="" alt=""/>
                  </div>
                  <div className='search-right'>
                      <Search
                          placeholder="搜索 天猫 商品/品牌/店铺"
                          enterButton="搜索"
                          size="large"
                          onSearch={value => console.log('aaa')}
                          style={{
                              fontSize: 16,
                              color: '#f00',
                          }}
                      />
                  </div>
              </div>

            </div>
        </div>

    )


}
export default HomeSearch