//轮播图
import { Carousel } from 'antd';
import React from "react";
import './tab.less'
const CarouselMy = () =>{

    return(
                <Carousel autoplay  className='carousel'>
                    <div>
                        <h3>
                            <img src={require('./image/a02ws-qclkd.jpg')}  alt="logo" />
                        </h3>
                    </div>
                    <div>
                        <h3><img src={require('./image/aoc3y-6mxn5.jpg')}  alt="logo" /></h3>
                    </div>
                    <div>
                        <h3><img src={require('./image/aw8zh-hw7r8.jpg')}  alt="logo" /></h3>
                    </div>
                    <div>
                        <h3><img src={require('./image/TB1w.FwEeL2gK0jSZFmSuw7iXXa.jpg')}  alt="logo" /></h3>
                    </div>
                    <div>
                        <h3><img src={require('./image/TB1Wi9BPrY1gK0jSZTESutDQVXa.jpg')}  alt="logo" /></h3>
                    </div>
                    <div>
                        <h3><img src={require('./image/TB17LX2da6qK1RjSZFmSut0PFXa.jpg')}  alt="logo" /></h3>
                    </div>
                </Carousel>
    )


}
export default CarouselMy