import React from 'react';
import {Menu} from 'antd'
import './head.css'
import '../../../layout/reset.css'
const { SubMenu } = Menu;
const Head = ()=>{

    return(
        <div id="container">
           <div id="main">
               <div className="headBar">
                   <Menu mode="horizontal" className='headBar floutLeft'>
                       <Menu.Item key="mail1">
                           天喵首页
                       </Menu.Item>
                       <Menu.Item key="mail2">
                           欢迎来到天喵
                       </Menu.Item>
                       <Menu.Item key="mail3">
                           请登录
                       </Menu.Item>
                       <Menu.Item key="mail4">
                           免费注册
                       </Menu.Item>
                   </Menu>
                   <Menu mode="horizontal" className=' headBar floutRight'>
                       <SubMenu title="我的淘宝">
                           <Menu.ItemGroup >
                               <Menu.Item key="setting:1">已买到的宝贝</Menu.Item>
                               <Menu.Item key="setting:2">已卖出的宝贝</Menu.Item>
                           </Menu.ItemGroup>
                       </SubMenu>
                       <Menu.Item key="mail1">
                           购物车
                       </Menu.Item>
                       <SubMenu title="收藏夹">
                           <Menu.ItemGroup title="">
                               <Menu.Item key="setting:1">收藏的宝贝</Menu.Item>
                               <Menu.Item key="setting:2">收藏的店铺</Menu.Item>
                           </Menu.ItemGroup>

                       </SubMenu>
                       <Menu.Item key="mail3">
                           手机版
                       </Menu.Item>
                       <Menu.Item key="mail4">
                           淘宝网
                       </Menu.Item>
                       <SubMenu title="商家支持">
                           <Menu.ItemGroup title="Item 1">
                               <Menu.Item key="setting:1">Option 1</Menu.Item>
                               <Menu.Item key="setting:2">Option 2</Menu.Item>
                           </Menu.ItemGroup>
                       </SubMenu>
                       <SubMenu title="网站导航">
                           <Menu.ItemGroup title="Item 1">
                               <Menu.Item key="setting:1">Option 1</Menu.Item>
                               <Menu.Item key="setting:2">Option 2</Menu.Item>
                           </Menu.ItemGroup>
                       </SubMenu>
                   </Menu>
               </div>
           </div>

        </div>

    )
}
export default Head;



