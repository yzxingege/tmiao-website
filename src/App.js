import React from 'react';
import './App.less';
import Index from "./pages/home";

function App() {

  return (
      <>
        <Index />
      </>
  );
}

export default App;
